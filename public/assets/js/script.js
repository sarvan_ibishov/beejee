/**
 * Created by Sarvan Ibishov on 8/19/2017.
 */
$(document).ready(function () {
    $("#button_Preview").click(function () {
        var InputName = $('input[name="username"]').val();
        var InputEmail = $('input[name="email"]').val();
        var InputImg = $('.upload_img_wrapper').html();
        var InputTask = $('textarea[name="body"]').val();

        html = "<div><b>Имя:</b>" + InputName + "</div>";
        html += "<div><b>Email:</b>" + InputEmail + "</div>";
        html += '<b>Image:</b> <div class="upload_img_wrapper">' + InputImg + '</div>';
        html += "<div><b>Task:</b>" + InputTask + "</div>";
        $("#toPreview").html(html);

        /* $.post('/ajax/new_task_preview.php',
         {InputName:InputName,InputEmail:InputEmail,InputImg:InputImg,InputTask:InputTask},
         function(data){
         $("#toPreview").html(data);
         }
         );*/
    });

    $('input[type="file"][preview-target-id]').on('change', function () {
        var input = $(this);
        console.log(input[0].files[0]);
        if (!window.FileReader) return false // check for browser support
        if (input[0].files && input[0].files[0]) {
            var reader = new FileReader()
            reader.onload = function (e) {
                var target = $('#' + input.attr('preview-target-id'))
                var background_image = 'url(' + e.target.result + ')'
                target.css('background-image', background_image)
                target.parent().show()
            }
            reader.readAsDataURL(input[0].files[0]);
        }
    });

    $(document).ready(function () {

        var dataTable = $('#employee-grid').DataTable({
            "processing": true,
            "serverSide": true,
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [0,3,5]}
            ],
            searching: false,
            "pageLength": 3,
            "lengthChange": false,
            columns: [
                {
                    "className": 'options',
                    "data": "image",
                    "render": function (data, type, full, meta) {

                        return '<img class="img-responsive" src="' + data + '"/>';
                    }
                },
                {"data": 'username'},
                {"data": 'email'},
                {"data": 'body'},
                {
                    "className": 'options',
                    "data": "status",
                    "render": function (data, type, full, meta) {

                        if (full.status == 'confirmed') {
                            return '<span class="label label-success">Выполнено</span>';
                        } else {
                            return '<span class="label label-warning">Выполняется</span>';
                        }
                    }
                },
                {
                    "className": 'options',
                    "data": null,
                    "render": function (data, type, full, meta) {
                        ;
                        if (a) {
                            return '<a href="/tasks/view/' + data.id + '" class="btn btn-primary">Show</a><a href="/tasks/edit/' + data.id + '" class="btn btn-success">Edit</a>';
                        } else {
                            return '<a href="/tasks/view/' + data.id + '" class="btn btn-primary">Show</a>';
                        }
                    }
                }
            ],
            "ajax": {
                url: "/datatable", // json datasource
                type: "get",  // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");

                }
            }
        });
    });
});