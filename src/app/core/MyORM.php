<?php

namespace App\Core;

use App\Config\Connect;

class MyORM extends Connect
{

    protected $connect;


    public function __construct()
    {

        $this->connect = parent::getDb();

    }

    protected function select($fields = null, $table = null, $cond = '', $limit = null, $count = null)
    {

        if ($table == null)
            $table = get_called_class();

        if ($fields == null)
            $fields = '*';
        else
            $fields = join(',', $fields);

        if ($limit !== null) {
            $cond .= " LIMIT $limit";
            if ($count !== null)
                $cond .= ", $count";
        }

        $query = "SELECT $fields FROM $table $cond ";


        $preparedStatement = $this->connect->prepare($query);
        $preparedStatement->execute();
        $result = $preparedStatement->fetchAll();
        return $result;
    }

    protected function getAll_()
    {
        return $this->select(null, get_called_class());
    }

    protected function insert($table, $values)
    {
        $fields = Array();
        $tempvalues = Array();
        foreach ($values as $key => $value) {
            $fields[] = $key;
            $tempvalues[] = ':' . $key;
        }

        $sql = "INSERT INTO $table(" . join(',', $fields) . ") VALUES (" . join(',', $tempvalues) . ")";


        $stmt = $this->connect->prepare($sql);

        foreach ($values as $key => $value) {
            $stmt->bindValue(":$key", $value);
        }

        $stmt->execute();
    }

    protected function delete($table, $cond)
    {

        $sql = "DELETE FROM $table WHERE " . $cond;


        $stmt = $this->connect->prepare($sql);

        $stmt->execute();
    }

    protected function query($sql)
    {
        echo $sql;
        $stmt = $this->connect->prepare($sql);

        return $stmt->execute();

    }

    protected function rawQuery($sql)
    {
        // Logger::q($sql);
        $preparedStatement = $this->connect->prepare($sql);
        //var_dump($preparedStatement);
        $preparedStatement->execute();
        $result = $preparedStatement->fetchAll();
        return $result;

    }


}


?>