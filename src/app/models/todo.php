<?php
namespace App\Model;

use App\Core\SimpleImage;

class Todo
{
    public $id;
    public $username;
    public $email;
    public $body;
    public $status;
    public $image;
    public $created_at;

    public function imageProcessing()
    {
        $uploaddir = 'public/uploads/';
        $uploadfile = $uploaddir . basename($_FILES['image']['name']);
        $imageFileType = pathinfo($uploadfile, PATHINFO_EXTENSION);

        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            throw new \Exception("Sorry, only JPG, JPEG, PNG & GIF files are allowed.", 1);
        }

        if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
            echo "Upload successful";
        } else {
            echo "File wasnt uploaded\n";
        }

        $image = new SimpleImage();
        $image->load($uploadfile);
        $image->resize(320, 240);
        $image->save($uploadfile);

        return $uploadfile;
    }


}
