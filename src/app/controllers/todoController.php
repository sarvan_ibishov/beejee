<?php

namespace App\Controller;

use App\Core\Controller;
use App\Model\Todo;
use App\Service\TodoService;


class TodoController extends Controller
{
    protected $todoService;

    function __construct()
    {
        parent::__construct();
        $this->todoService = new TodoService();
        /*$this->authService = new AuthService();*/
    }

    public function create()
    {

        $this->view->render('create.html.twig');
    }

    public function store()
    {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $body = $_POST['body'];

        $todo = new Todo();
        $todo->email = $email;
        $todo->body = $body;
        $todo->username = $username;
        $todo->image = $todo->imageProcessing();
        $todo->created_at = time();
        $todo->status = 'pending';
        $this->todoService->add($todo);
        header("Location:/");
    }

    public function index()
    {


        $todos = $this->todoService->getAll();
        $data = array('comments' => $todos);


        $this->view->render('todos.html.twig', $data);
    }

    public function view($id)
    {

        $todo = $this->getOne($id);

        $this->view->render('view.html.twig', $todo[0]);
    }

    public function edit($id)
    {

        $todo = $this->getOne($id);
        $this->view->render('edit.html.twig', $todo[0]);
    }

    public function update()
    {
        $id = (int)$_POST['id'];
        $body = htmlspecialchars($_POST['body']);
        $status = htmlspecialchars($_POST['status']);
        $this->todoService->update($id, $body, $status);
        header("Location:/");

    }

    public function getOne($id)
    {
        return $this->todoService->getOne($id);
    }

    public function datatable()
    {
         $this->todoService->datatable();
    }


}
