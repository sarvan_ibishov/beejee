<?php


namespace App\Controller;

use App\Core\Controller;

class AuthController extends Controller
{


    public function login()
    {

        // $userData = App::get('database')->selectUserFromDB('users', $_POST['name'], $_POST['password']);
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';
        $errors = $userData = [];
        if (empty($name) || empty($password)) {
            $errors['message'] = "Логин и/или пароль введены неверно.";
        }
        if ($name == 'admin' && $password == 123) {

            $userData['user'] = 'admin';

        } else {
            $errors['message'] = "Логин и/или пароль введены неверно.";
        }


        if (!empty($userData)) {

            $userData['success'] = true;
            $_SESSION['userData'] = $userData;
            header("Location:/");
            exit();
        }


        $this->view->render('login.html.twig', $errors);

    }

    public function login_view()
    {

        $this->view->render('login.html.twig');
    }

    public function logout()
    {

        unset($_SESSION['userData']);
        header("Location:/");
        exit();

    }
}