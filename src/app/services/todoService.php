<?php
namespace App\Service;


use App\Core\MyORM;
use PDO;

class TodoService extends MyORM
{
    //protected $connect;

    function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {

        $data = $this->select(null, 'todos');
        return $data;


    }

    public function add($todo)
    {

        $this->insert('todos', $todo);
    }

    public function getOne($id)
    {
        return $this->select(null, 'todos', 'where id=' . (int)$id);
    }

    public function update($id, $body, $status)
    {
        $this->query("Update todos set body='{$body}',status='{$status}' where id=$id");
    }

    public function datatable()
    {
        $requestData = $_REQUEST;
        $columns = array(
// datatable column index  => database column name
            0 => 'image',
            1 => 'username',
            2 => 'email',
            3 => 'body',
            4 => 'status',


        );

        $totalData = count($this->getAll());
        $totalFiltered = $totalData;
        $data = $this->select(array('id', 'image', 'username', 'email', 'body', 'status'), 'todos', "ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "", $requestData['start'], $requestData['length']);


        $json_data = array(
            "draw" => intval($requestData['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),  // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );
        echo json_encode($json_data);  // send data as json format
    }
}
