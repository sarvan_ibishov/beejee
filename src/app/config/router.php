<?php
namespace App\Config;
use AltoRouter;

class Router
{
    protected $router;

    function __construct()
    {
        $this->router = new AltoRouter();


        $this->router->map('get', '/', function () {
            $this->routingController('TodoController', 'index');
        });
        $this->router->map('get', '/datatable', function () {
            $this->routingController('TodoController', 'datatable');
        });
        $this->router->map('get', '/tasks/create', function () {
            $this->routingController('TodoController', 'create');
        });
        $this->router->map('post', '/tasks/store', function () {
            $this->routingController('TodoController', 'store');
        });
        $this->router->map('get', '/tasks/view/[i:id]', function ($id) {
            $this->routingController('TodoController', 'view', $id);
        });
        $this->router->map('get', '/tasks/edit/[i:id]', function ($id) {
            $this->routingController('TodoController', 'edit', $id);
        });
        $this->router->map('post', '/tasks/update', function () {
            $this->routingController('TodoController', 'update');
        });
        $this->router->map('get', '/login', function () {
            $this->routingController('AuthController', 'login_view');
        });
        $this->router->map('get', '/logout', function () {
            $this->routingController('AuthController', 'logout');
        });
        $this->router->map('post', '/login', function () {
            $this->routingController('AuthController', 'login');
        });


        //Пути роутинга прописать
    }

    public function routing()
    {
        $match = $this->router->match();
        $target = $match['target'];
        $param = $match['params'];

       // print_r($match);

        if ($match && is_callable($match['target'])) {
            call_user_func_array($match['target'], $match['params']);
        } else {
            // no route was matched
            $this->NotFound();
        }
    }

    //Не доработанная функция на ошибок
    public function routingController($controllerName, $action, $param = null)
    {

        $controllerName = "App\\Controller\\" . $controllerName;
        if (class_exists($controllerName)) {
            $controller = new $controllerName;
            if (method_exists($controller, $action)) {
                if (is_null($param)) {
                    $controller->$action();
                } else {
                    $controller->$action($param);
                }
            }
        } else {
            $this->NotFound();
        }
    }

    public function NotFound()
    {
        header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
        include('src/404.php');
    }
}

?>
